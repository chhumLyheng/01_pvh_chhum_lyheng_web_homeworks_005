import React, { Component } from 'react'
import MainComponents from './Components/MainComponents'

export class App extends Component {
  render() {
    return (
      <div>
        <MainComponents/>
      </div>
    )
  }
}

export default App