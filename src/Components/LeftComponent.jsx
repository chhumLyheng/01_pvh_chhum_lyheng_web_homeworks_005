import React from "react";
import category_icon from '../Images/category_icon.png';
import cube from  '../Images/cube.png';
import list from  '../Images/list.png';
import messenger from  '../Images/messenger.png';
import list1 from  '../Images/list.png';
import success from  '../Images/success.png';
import notification from  '../Images/notification.png';
import users from  '../Images/users.png';
import raamin from  '../Images/raamin.jpg';
import nonamesontheway from  '../Images/nonamesontheway.jpg';
import lachlan from  '../Images/lachlan.jpg';
import plus from  '../Images/plus.png';


export const LeftComponent = () => {
  return (
    <div>
      {/* category_icon */}
      <aside className="">
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={category_icon} alt="category_icon" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* cube */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={cube} alt="cube" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* list */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <div className=" bg-red-600 w-2 h-2 absolute rounded-3xl ml-3"/>
                <img src={list} alt="list" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/*  messenger*/}
      <aside>
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
              <div className=" bg-red-600 w-2 h-2 absolute rounded-3xl ml-3"/>
                <img src={messenger} alt="imgs" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* list1 */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={list1} alt="list1" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside><br/>

{/* success */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={success} alt="imgs" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* otification */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={notification} alt="imgs" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

      {/* users */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={users} alt="imgs" className="w-5 h-5" />
              </li>
            </ul>
          </div>
        </div>
      </aside><br/>
<div>


{/* raamin */}
 <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={raamin} alt="imgs" className="w-5 h-5 rounded-full" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* nonamesontheway */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto ">
          <div>
            <ul>
              <li>
                <img src={nonamesontheway} alt="imgs" className="w-5 h-5 rounded-full" />
              </li>
            </ul>
          </div>
        </div>
      </aside>

{/* lachlan */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={lachlan} alt="imgs" className="w-5 h-5 rounded-full " />
              </li>
            </ul>
          </div>
        </div>
      </aside>
</div>
     
{/* plus */}
      <aside >
        <div class=" h-full px-3 py-4 overflow-y-auto">
          <div>
            <ul>
              <li>
                <img src={plus} alt="imgs" className="w-5 h-5 " />
              </li>
            </ul>
          </div>
        </div>
      </aside>
      
    </div>
  );
};
