import React from "react";
import lachlan from "../Images/lachlan.jpg";
import raamin from "../Images/raamin.jpg";
import nonamesontheway from "../Images/nonamesontheway.jpg";
import christina from "../Images/christina.jpg";

export const RightComponent = () => {
  return (
    <div className="bg-rightImage w-96 min-h-screen bg-no-repeat ">
      <div className="bg-lime-100 rounded-2xl absolute p-3.5 mr-9 mt-40 ml-16 font-bold">
        <h2 className="text-center ">My Amazing Trip</h2>
      </div>

      {/* Text */}
      <div className="absolute p-2 mt-52 px-5  text-white ">
        <p className="text-3xl"> I like laying down on </p>
        <p className="text-3xl">the sand and looking at</p>
        <p className="text-3xl">the moon.</p> <br /><br/>
        <p>27 people going to this trip</p>

        {/* poeple going */}
        <div className="absolute p-2 mt-5 grid grid-cols-5  w-19 h-19  ">
          <img
            src={lachlan}
            alt="imgs"
            className="w-10 h-10 rounded-full ml-2 mr-2  " />
          <img
            src={nonamesontheway}
            alt="imgs"
            className="w-10 h-10 rounded-full"/>
          
          <img src={raamin} alt="imgs" className="w-10 h-10 rounded-full" />
          <img src={christina} alt="imgs" className="w-10 h-10 rounded-full" />
          <div>
            <p className="bg-red-300 absolute  text-red-500 font-bold w-10 h-10 border-2 rounded-3xl text-center">
              23+
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
