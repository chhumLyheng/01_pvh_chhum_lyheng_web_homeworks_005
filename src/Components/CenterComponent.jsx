import React, { useState } from "react";

export const CenterComponent = ({ traval, setTravel }) => {
  const [newTrip, setNewTrip] = useState([]);
  const [readDetails, setReadDetails] = useState([]);

  const inputHandler = (e) => {
    setNewTrip({ ...newTrip, [e.target.name]: e.target.value });
    console.log(newTrip);
  };

  const submitHadler = (e) => {
    e.preventDefault();
    setTravel([...traval, { id: traval.length + 1, ...newTrip }]);
    console.log(traval);
  };
  const readDetail = (e) => {
    setReadDetails(e);
  };

  return (
    <div>
      <div class="flex w-full grow ">
        <div class="pl-4 m-8   font-bold rounded-lg text- px-9 py-3.5 mr-2 mb-2 flex-1  text-5xl">
          Good Evening Team!
        </div>

        {/*Button Add New Trip  */}
        <label htmlFor="my-modal-3" className="btn m-8">
          Add New Trip
        </label>
        <input type="checkbox" id="my-modal-3" className="modal-toggle" />
        <div className="modal">
          <div className="modal-box relative">
            <label
              htmlFor="my-modal-3"
              className="btn btn-sm btn-circle absolute right-2 top-2"
            >
              ✕
            </label>

            {/* Input */}
            <div>
              <h3 className="font-medium text-lg">Title</h3>
              <input
                type="text"
                name="title"
                placeholder="Sihaknou Ville"
                className="input input-bordered input-sm w-full max-w-xs"
                onChange={inputHandler}
              />

              <h3 className="font-medium  m-2 text-lg">Description</h3>
              <input
                type="text"
                name="description"
                placeholder="Happy place with beautiful Beach"
                className="input input-bordered input-sm w-full max-w-xs"
                onChange={inputHandler}
              />

              <h3 className="font-medium  m-2 text-lg">People Going</h3>
              <input
                type="text"
                name=" peopleGoing"
                placeholder="3200"
                className="input input-bordered input-sm w-full max-w-xs"
                onChange={inputHandler}
              />
              <h3 className="font-medium  m-2 text-lg">Type Adventure</h3>
              <select
                className="select select-primary w-full max-w-xs"
                onChange={inputHandler}
              >
                <option disabled selected>
                  .....Choose option here.....
                </option>
                <option>Beach</option>
                <option>Mountaint</option>
                <option>Forest</option>
              </select>
            </div>

            {/* Submit */}
            <div className="modal-action">
              <label
                htmlFor="my-modal-3"
                className="btn"
                onClick={submitHadler}
              >
                Submit
              </label>
            </div>
          </div>
        </div>
      </div>

      {/* Read Detail */}
      <div className="grid grid-cols-3 gap-5 text-white ">
        {traval.map((e) => (
          <div className="pl-4 bg-teal-800 rounded-lg h-70 mr-6">
            <h2 className="card-title diagonal-fractions mt-5">{e.title}</h2>
            <br />
            <p className="line-clamp-3 italic hover:not-italic scroll-py-9">
              {e.description}
            </p>
            <p className="line-clamp-3 italic hover:not-italic scroll-py-9 p-5">
              People Going
              <br /> {e.peopleGoing}
            </p>

            <div className="card-actions justify-end">
              <div className=" pr-9 grid grid-cols-2 gap-9 ">
               <div className="m-2">
               <button className="bg-lime-400  hover:bg-blue-700 text-white font-medium  mt-2 py-2 px-4 rounded w-32 h-9 scroll-pb-5 ">
                  {e.status}
                </button>
               </div>
                
                <label htmlFor="readDetail" className="btn w-32 h-9  mt-2">
                  READ DETAILS
                </label>

                <input
                  type="checkbox"
                  id="readDetail"
                  className="modal-toggle"
                />
                <div className="modal">
                  <div className="modal-box relative">
                    <label
                      htmlFor="readDetail"
                      className="btn btn-sm btn-circle absolute right-2 top-2"
                      onClick={() => readDetail(e)}
                    >
                      ✕
                    </label>
                    <div className="text-yellow-50 bg-teal-500">
                    <h1 className="font-medium pt-2 px-3 text-lime-300 ">
                      {readDetails.title}
                    </h1>
                <p className="font-medium pt-2 text-white py-3 px-4 italic">
                  {readDetails. description}
                 
                </p>
                <p className="font-medium pt-2 text-white py-3 px-4 italic">
               Around {readDetails.peopleGoing} people going there.
                </p>
                    </div>
                  </div>
                </div>
              
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
